'use strict';

// ready
$(document).ready(function() {


    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $('body').addClass('fix');
        $(this).next().slideToggle();
    });
    $('.main-nav__toggle--jsf').click(function () {
        $(this).parent().toggleClass('active');
        $(this).next().slideToggle();
    });
    $('.main-nav__close--js').click(function () {
        $('body').removeClass('fix');
        $(this).closest('.main-nav__collapse').slideToggle();
        return false;
    });
    // adaptive menu

    //adaptive category
    $('.mm-category__item-openlink--js').click(function () {
        $(this).parent().addClass('active');
        return false;
    });
    $('.mm-category__item-closelink').click(function () {
        $(this).closest('.mm-category__item--js').removeClass('active');
        return false;
    });
    //adaptive category

    //adaptive search
    $('.search-mobile--js, .time--js').click(function () {
        $(this).next().slideToggle();
        return false;
    });
    $('.close--js').click(function () {
        $(this).parent().parent().hide();
        return false;
    });
    //adaptive search

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    //$('.slider').slick({});
    $('.carousel').slick({
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.carousel-obj').slick({
        slidesToShow: 6,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
    // slider

    // select {select2}
    function formatState (state) {
        if (!state.id) { return state.text; }
        var $state = $(
            '<a href="' + state.element.value.toLowerCase() + '.html" class="link-link">' + state.text + '</a>'
        );
        return $state;
    };
    var select2 = $('select').select2({
        placeholder: "",
        templateResult: formatState,
        templateSelection: formatState,
        escapeMarkup: function(m) { return m; }
    }).data('select2');

    $(".control-header").select2({
        minimumResultsForSearch: Infinity
    });
    $('.control-hard').on("select2:select", function (e) {
        var target = $(this).parent().find('link-link');
        var classSelect = $(this).parent().find('.select2');
        var cusSelect = $(this).parent().find('.select2-selection__rendered').attr('title');
        var dataLink = $(target.selector).attr('href');
        if (cusSelect == '') {
            $(this).removeClass('edited');
            classSelect.removeClass('edited');
        } else {
            $(this).addClass('edited');
            classSelect.addClass('edited');
        }
        if (target.selector == 'link') {
            console.log(target.element.value);
            // console.log(target.selector);
            // return true;
        } else {
            console.log(target);
        }
    });
    $('.label-hard').click(function () {
        $(this).parent().find('.select-hard').select2('open');
    });
    // select

    // floating labels
    var body = $('body');
    var handleInput = function(el) {
        if (el.val() != "") {
            el.addClass('edited');
        } else {
            el.removeClass('edited');
        }
    };
    body.on('keydown', '.control', function(e) {
        handleInput($(this));
    });
    body.on('blur', '.control', function(e) {
        handleInput($(this));
    });
    // floating labels

    // popup {magnific-popup}
    $('.popup').magnificPopup({
        showCloseBtn: false
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    // popup
});
// ready


// load
$(window).load(function() {
    //masonry
    $('.masonry').masonry({
        itemSelector: '.cell',
        isResizable: true
    });
    //masonry
});
// load

// scroll
$(window).scroll(function() {
    var sticky = $('.page-header-fixed'),
        scroll = $(window).scrollTop();
    if (scroll >= 60) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
});
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts


function initMap() {
    var uluru = {lat: 59.91916157, lng:  30.3251195};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        zoomControl: false,
        scaleControl: false,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        center: uluru
    });

    var contentString = '<div class="org-info">'+
        '<p><i class="icon icon-location_icon"></i> Индустриальный пр., 17, к. 1, литер А</p>'+
        '<p><i class="icon icon-phone_icon"></i> (812) 323-23-23</p></div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "images/pin.png"
    });
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
}
